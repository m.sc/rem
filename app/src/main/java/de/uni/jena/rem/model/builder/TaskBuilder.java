package de.uni.jena.rem.model.builder;

import java.util.Date;
import java.util.List;
import java.util.Set;

import de.uni.jena.rem.model.Tag;
import de.uni.jena.rem.model.Task;

/**
 * Created by msc on 02.05.17.
 */

public class TaskBuilder
{
    private String title;
    private String message;
    private Tag tag;
    private String description;
    private String wlanUid;

    public TaskBuilder title(final String title)
    {
        this.title = title;
        return this;
    }

    public TaskBuilder message(final String message)
    {
        this.message = message;
        return this;
    }

    public TaskBuilder tag(final Tag tag)
    {
        this.tag = tag;
        return this;
    }

    public TaskBuilder description(final String description)
    {
        this.description = description;
        return this;
    }

    public TaskBuilder wlanUid(final String wlanUid)
    {
        this.wlanUid = wlanUid;
        return this;
    }

    public Task build()
    {
        Task task = new Task();

        if(this.title == null || this.message == null)
        {
            throw new IllegalArgumentException("Either title["+ this.title +"] or message["+
                    this.message +"] was null");
        }

        task.setTitle(this.title);
        task.setMessage(this.message);
        task.setTag(this.tag);
        task.setDescription(this.description);
        task.setDone(Boolean.FALSE);
        task.setCreationTime(new Date());
        task.setWlanUid(this.wlanUid);

        return task;
    }
}
