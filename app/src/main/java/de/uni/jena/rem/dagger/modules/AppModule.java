package de.uni.jena.rem.dagger.modules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by msc on 07.05.17.
 */
@Module
public class AppModule
{
    Application application;

    public AppModule(Application application)
    {
        this.application = application;
    }

    @Provides
    @Singleton
    Application providesApplication()
    {
        return this.application;
    }
}
