package de.uni.jena.rem.service;

import android.app.IntentService;

import android.app.Notification;
import android.app.NotificationManager;

import android.content.Intent;
import android.net.ConnectivityManager;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;

import java.util.List;

import javax.inject.Inject;

import de.uni.jena.rem.RemApplication;
import de.uni.jena.rem.dagger.component.RemComponent;
import de.uni.jena.rem.helper.TaskHelper;
import de.uni.jena.rem.model.Task;
import rem.jena.uni.com.rem.R;

/**
 * Created by msc on 15.07.17.
 */

public class TaskReminderService extends IntentService
{
    @Inject
    NotificationManager notificationManager;
    @Inject
    WifiManager wifiManager;
    @Inject
    ConnectivityManager connectivityManager;
    @Inject
    TaskHelper taskHelper;

    public TaskReminderService()
    {
        super("TaskReminderService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((RemApplication) getApplication()).getRemComponent().inject(this);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent)
    {
        List<Task> allTasks = taskHelper.getAllTasks();
        if(allTasks != null && !allTasks.isEmpty())
        {
            final String connectedWifiSSID = getWifiSSID();

            for(Task task : allTasks)
            {
                //this should be done in a generic extendible way if there are more fields to be checked
                if(task.getWlanUid() != null && !task.getWlanUid().isEmpty() &&
                        task.getWlanUid().equals(connectedWifiSSID))
                {
                    displayNotificationForTask(task);
                }
            }
        }
    }

    private void displayNotificationForTask(final Task task)
    {
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.rem)
                .setContentTitle(this.getResources().getString(R.string.notification_Title_wlan))
                .setContentText(task.getTitle())
                .build();
        //ids dont matter bc we dont want to cancel the notification programmatically
        notificationManager.notify(0, notification);
    }

    //returns Wifi SSID if Connected
    //Optionals would be great now there needs to be an null check
    private String getWifiSSID()
    {
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        //wifiInfo.getNetworkId() == -1 when not connected to anything
        if(wifiManager.isWifiEnabled() && wifiInfo.getNetworkId() != -1)
        {
            return wifiInfo.getSSID();
        }
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public NotificationManager getNotificationManager()
    {
        return notificationManager;
    }

    public void setNotificationManager(NotificationManager notificationManager)
    {
        this.notificationManager = notificationManager;
    }

    public TaskHelper getTaskHelper() {
        return taskHelper;
    }

    public void setTaskHelper(TaskHelper taskHelper) {
        this.taskHelper = taskHelper;
    }

    public WifiManager getWifiManager() {
        return wifiManager;
    }

    public void setWifiManager(WifiManager wifiManager) {
        this.wifiManager = wifiManager;
    }

    public ConnectivityManager getConnectivityManager() {
        return connectivityManager;
    }

    public void setConnectivityManager(ConnectivityManager connectivityManager) {
        this.connectivityManager = connectivityManager;
    }
}
