package de.uni.jena.rem.listeners;

import android.content.Intent;
import android.view.View;

import de.uni.jena.rem.activity.TaskInspectActivity;
import de.uni.jena.rem.activity.TaskListActivity;
import de.uni.jena.rem.model.Task;
import rem.jena.uni.com.rem.R;

/**
 * Created by msc on 03.05.17.
 */

public class TaskItemOnLongClickListener implements View.OnLongClickListener
{
    private Long longClickedTaskId;

    @Override
    public boolean onLongClick(View v)
    {
        final String intentTaskIdExtraId = v.getContext().getResources()
                .getString(R.string.intentTaskId);

        Intent inspectTaskActivityIntent = new Intent(v.getContext().getApplicationContext(),
                TaskInspectActivity.class);
        inspectTaskActivityIntent.putExtra(intentTaskIdExtraId, longClickedTaskId);

        v.getContext().startActivity(inspectTaskActivityIntent);

        return true;
    }

    public void setLongClickedTaskId(Long longClickedTaskId) {
        this.longClickedTaskId = longClickedTaskId;
    }
}
