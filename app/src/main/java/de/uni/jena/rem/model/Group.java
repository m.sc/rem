package de.uni.jena.rem.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;
import org.greenrobot.greendao.DaoException;

/**
 * Created by msc on 16.05.17.
 */
@Entity
public class Group
{
    @Id(autoincrement = true)
    private Long id;

    private String name;

    @Generated(hash = 1912365892)
    public Group(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Generated(hash = 117982048)
    public Group() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
