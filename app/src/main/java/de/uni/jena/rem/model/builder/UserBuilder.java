package de.uni.jena.rem.model.builder;

import java.util.List;

import de.uni.jena.rem.model.Group;
import de.uni.jena.rem.model.User;

/**
 * Created by msc on 16.05.17.
 */

public class UserBuilder
{
    private String firstName;
    private String lastName;

    public UserBuilder firstName(String firstName)
    {
        this.firstName = firstName;
        return this;
    }

    public UserBuilder lastName(String lastName)
    {
        this.lastName = lastName;
        return this;
    }

    public User build()
    {
        User user = new User();
        user.setFirstName(this.firstName);
        user.setLastName(this.lastName);


        return user;
    }
}
