package de.uni.jena.rem.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by msc on 02.05.17.
 */
@Entity
public class Tag
{
    @Id(autoincrement = true)
    private Long id;

    private String name;

    @Generated(hash = 1359362089)
    public Tag(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Generated(hash = 1605720318)
    public Tag() {
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
