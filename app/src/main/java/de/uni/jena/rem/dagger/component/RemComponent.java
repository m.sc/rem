package de.uni.jena.rem.dagger.component;

import javax.inject.Singleton;

import dagger.Component;
import de.uni.jena.rem.activity.LoginActivity;
import de.uni.jena.rem.activity.TaskInspectActivity;
import de.uni.jena.rem.activity.TaskListActivity;
import de.uni.jena.rem.dagger.modules.AppModule;
import de.uni.jena.rem.dagger.modules.DaoModule;
import de.uni.jena.rem.dagger.modules.RemModule;
import de.uni.jena.rem.fragment.TagSpinnerFragment;
import de.uni.jena.rem.service.TaskReminderService;

/**
 * Created by msc on 07.05.17.
 */

@Singleton
@Component(modules = {AppModule.class, DaoModule.class, RemModule.class})
public interface RemComponent
{
    void inject(TaskListActivity activity);
    void inject(TaskInspectActivity activity);
    void inject(LoginActivity activity);

    void inject(TaskReminderService service);

    void inject(TagSpinnerFragment fragment);
}
