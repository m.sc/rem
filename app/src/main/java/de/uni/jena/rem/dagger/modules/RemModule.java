package de.uni.jena.rem.dagger.modules;

import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.uni.jena.rem.adapter.TaskListAdapter;
import de.uni.jena.rem.dialog.CreateTaskDialog;
import de.uni.jena.rem.helper.RestHelper;
import de.uni.jena.rem.helper.TagHelper;
import de.uni.jena.rem.helper.TaskHelper;
import de.uni.jena.rem.helper.impl.DefaultRestHelper;
import de.uni.jena.rem.helper.impl.DefaultTagHelper;
import de.uni.jena.rem.listeners.AddTaskButtonOnClickListener;
import de.uni.jena.rem.listeners.TaskItemOnLongClickListener;
import de.uni.jena.rem.model.TagDao;
import de.uni.jena.rem.model.Task;
import de.uni.jena.rem.model.TaskDao;
import de.uni.jena.rem.helper.impl.DefaultTaskHelper;
import de.uni.jena.rem.service.TaskReminderService;
import rem.jena.uni.com.rem.R;

/**
 * Created by msc on 07.05.17.
 */

@Module
public class RemModule
{
    @Provides
    @Singleton
    List<String> provideAvailableTags(Application app)
    {
        return Arrays.asList(app.getResources().getStringArray(R.array.tags));
    }

    @Provides
    @Singleton
    ArrayList<Task> provideTaskArrayList(TaskDao taskDao)
    {
        ArrayList<Task> taskList = new ArrayList<>();
        //taskList.addAll(taskDao.loadAll());
        return taskList;
    }

    @Provides
    @Singleton
    TaskListAdapter providesTaskArrayAdapter(Application app, ArrayList<Task> taskList,
                                             TaskItemOnLongClickListener clickListener)
    {
        TaskListAdapter taskArrayAdapter = new TaskListAdapter(app,
                R.layout.fragment_tasklist_item, taskList);
        taskArrayAdapter.setOnLongClickListener(clickListener);
        return taskArrayAdapter;
    }

    @Provides
    TaskHelper provideTaskService(TaskListAdapter taskListAdapter, TaskDao taskDao,
                                  TagHelper tagService)
    {
        DefaultTaskHelper taskService = new DefaultTaskHelper();
        taskService.setTaskListAdapter(taskListAdapter);
        taskService.setTaskDao(taskDao);
        taskService.setTagService(tagService);

        return taskService;
    }

    @Provides
    TagHelper provideTagService(List<String> availableTags, TagDao tagDao)
    {
        DefaultTagHelper tagService = new DefaultTagHelper();
        tagService.setTags(availableTags);
        tagService.setTagDao(tagDao);

        return tagService;
    }

    @Provides
    AddTaskButtonOnClickListener providesAddTaskOnClickListener(TaskHelper taskHelper,
                                                                TagHelper tagService,
                                                                TaskListAdapter taskListAdapter)
    {
        AddTaskButtonOnClickListener clickListener =  new AddTaskButtonOnClickListener();
        clickListener.setTaskHelper(taskHelper);
        clickListener.setTagService(tagService);
        clickListener.setTaskListAdapter(taskListAdapter);

        return clickListener;
    }

    @Provides
    TaskItemOnLongClickListener provideTaskItemOnLongClickListener()
    {
        TaskItemOnLongClickListener listener = new TaskItemOnLongClickListener();
        return listener;
    }

    @Provides
    @Singleton
    NotificationManager provideNotficationManager(Application application)
    {
        return (NotificationManager) application.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Provides
    @Singleton
    WifiManager provideWifiManager(Application application)
    {
        return (WifiManager) application.getSystemService(Context.WIFI_SERVICE);
    }

    @Provides
    @Singleton
    ConnectivityManager provideConnectivityManager(Application application)
    {
        return (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Provides
    @Singleton
    AlarmManager provideAlarmManager(Application application)
    {
        return (AlarmManager) application.getSystemService(Context.ALARM_SERVICE);
    }

    @Provides
    @Singleton
    TaskReminderService provideTaskReminderService(NotificationManager notificationManager, TaskHelper taskHelper)
    {
        TaskReminderService service = new TaskReminderService();
        service.setNotificationManager(notificationManager);

        return service;
    }

    @Provides
    RestHelper provideRestHandler()
    {
        RestHelper restHelper = new DefaultRestHelper();
        return restHelper;
    }
}
