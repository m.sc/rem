package de.uni.jena.rem.helper.impl;

import de.uni.jena.rem.helper.RestHelper;

/**
 * Created by msc on 15.06.17.
 */

public class DefaultRestHelper implements RestHelper
{

    @Override
    public boolean loginUser(String username, String password) {
        return true;
    }

    @Override
    public boolean registerUser(String username, String password) {
        return true;
    }
}
