package de.uni.jena.rem.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import de.uni.jena.rem.listeners.TaskItemOnLongClickListener;
import de.uni.jena.rem.model.Task;
import rem.jena.uni.com.rem.R;

/**
 * Created by msc on 02.05.17.
 */

public class TaskListAdapter extends ArrayAdapter<Task>
{
    @Inject TaskItemOnLongClickListener onLongClickListener;

    public TaskListAdapter(Context ctx, int textViewRessourceId)
    {
        super(ctx, textViewRessourceId);
    }

    public TaskListAdapter(Context ctx, int ressource, List<Task> tasks)
    {
        super(ctx, ressource, tasks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View rootView = convertView;

        if(rootView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            rootView = inflater.inflate(R.layout.fragment_tasklist_item, null);
        }

        final Task taskToShow = getItem(position);

        if(taskToShow != null && taskToShow.getMessage() != null)
        {
            TextView taskMessageView = (TextView) rootView.findViewById(R.id.showTaskMessage);
            TextView taskTagsView = (TextView) rootView.findViewById(R.id.showTaskTags);
            TextView taskDescriptionView = (TextView) rootView.findViewById(R.id.showTaskDescription);

            if(taskMessageView != null)
            {
                taskMessageView.setText(taskToShow.getMessage());
                
                List<View> toBeRemovedViews = new ArrayList<>();
                
                if(taskToShow.getTag() == null || taskToShow.getTag().getName().isEmpty())
                {
                    toBeRemovedViews.add(taskTagsView);
                }
                else
                {
                    taskTagsView.setText(taskToShow.getTag().getName());
                }

                if(taskToShow.getDescription() == null || taskToShow.getDescription().isEmpty())
                {
                    toBeRemovedViews.add(taskDescriptionView);
                }
                else
                {
                    taskDescriptionView.setText(taskToShow.getDescription());
                }

                setLongOnClickListenerForItem(rootView, position, taskToShow);
                removeUnusedViews(rootView, toBeRemovedViews);
            }

        }
        return rootView;
    }

    private void removeUnusedViews(View rootView, List<View> views)
    {
        for(View view : views)
        {
            ((ViewManager)rootView).removeView(view);
        }
    }

    private void setLongOnClickListenerForItem(final View rootView,final int position, final Task taskToShow)
    {
        TaskItemOnLongClickListener listener = new TaskItemOnLongClickListener();
        listener.setLongClickedTaskId(taskToShow.getId());
        rootView.setOnLongClickListener(listener);
    }

    public void setOnLongClickListener(TaskItemOnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }
}
