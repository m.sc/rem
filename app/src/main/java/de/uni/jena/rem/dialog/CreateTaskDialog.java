package de.uni.jena.rem.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import de.uni.jena.rem.adapter.TaskListAdapter;
import de.uni.jena.rem.helper.TagHelper;
import de.uni.jena.rem.helper.TaskHelper;
import de.uni.jena.rem.model.Tag;
import de.uni.jena.rem.model.Task;
import de.uni.jena.rem.model.builder.TagBuilder;
import de.uni.jena.rem.model.builder.TaskBuilder;
import rem.jena.uni.com.rem.R;

/**
 * Created by msc on 17.07.17.
 */

public class CreateTaskDialog extends Dialog implements View.OnClickListener
{
    private static final String LOG_TAG = CreateTaskDialog.class.getCanonicalName();

    TaskHelper taskHelper;
    TagHelper tagHelper;
    TaskListAdapter taskListAdapter;

    EditText title;
    EditText message;
    Spinner wlanSSID;
    Spinner tag;
    Button create;
    Button cancel;

    public CreateTaskDialog(Activity activity) {
        super(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_createtask);
        this.title = (EditText) findViewById(R.id.createTaskTitle);
        this.message = (EditText) findViewById(R.id.editTaskMessage);
        this.wlanSSID = (Spinner) findViewById(R.id.createTaskWlanSSID);
        addKnwonWifisToWifiSpinner();
        this.tag = (Spinner) findViewById(R.id.tagSpinner);

        this.create = (Button) findViewById(R.id.createTaskButton);
        this.create.setOnClickListener(this);

        this.cancel = (Button) findViewById(R.id.cancel);
        this.cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.createTaskButton)
        {
            try
            {
                Tag tag  = createTagFromSpinner();
                tagHelper.saveTag(tag);

                Task newTask = new TaskBuilder()
                        .title(this.title.getText().toString())
                        .message(this.message.getText().toString())
                        .tag(tag)
                        .wlanUid((String) this.wlanSSID.getSelectedItem())
                        .build();

                taskHelper.saveTask(newTask);
                taskListAdapter.add(newTask);
            }
            catch(IllegalArgumentException e)
            {
                Log.e(LOG_TAG, "Error while creating task!", e);
                dismiss();
            }
        }
        dismiss();
    }

    private void addKnwonWifisToWifiSpinner()
    {
        WifiManager wifiManager = (WifiManager) getContext().getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);

        List<String> configuredWifis = new ArrayList<>();
        List<WifiConfiguration> configs = wifiManager.getConfiguredNetworks();
        if(configs != null)
        {
            for(WifiConfiguration config : configs)
            {
                configuredWifis.add(config.SSID);
            }

            ArrayAdapter<String> wlanSSIDAdapter = new ArrayAdapter<String>(getOwnerActivity(),
                    android.R.layout.simple_list_item_1, configuredWifis);

            this.wlanSSID.setAdapter(wlanSSIDAdapter);
        }
    }

    private Tag createTagFromSpinner()
    {
        return new TagBuilder().name((String) this.tag.getSelectedItem()).build();
    }

    public TaskHelper getTaskHelper() {
        return taskHelper;
    }

    public void setTaskHelper(TaskHelper taskHelper) {
        this.taskHelper = taskHelper;
    }

    public TaskListAdapter getTaskListAdapter() {
        return taskListAdapter;
    }

    public void setTaskListAdapter(TaskListAdapter taskListAdapter) {
        this.taskListAdapter = taskListAdapter;
    }

    public TagHelper getTagHelper() {
        return tagHelper;
    }

    public void setTagHelper(TagHelper tagHelper) {
        this.tagHelper = tagHelper;
    }
}
