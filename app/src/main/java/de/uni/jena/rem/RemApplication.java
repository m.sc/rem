package de.uni.jena.rem;

import android.app.Application;

import org.greenrobot.greendao.database.Database;

import de.uni.jena.rem.dagger.component.DaggerRemComponent;
import de.uni.jena.rem.dagger.component.RemComponent;
import de.uni.jena.rem.dagger.modules.AppModule;
import de.uni.jena.rem.dagger.modules.DaoModule;
import de.uni.jena.rem.dagger.modules.RemModule;
import de.uni.jena.rem.model.DaoMaster;
import de.uni.jena.rem.model.DaoSession;

/**
 * Created by msc on 20.04.17.
 */

public class RemApplication extends Application
{
    private static final String LOG_TAG = RemApplication.class.getCanonicalName();

    public static final Boolean ENCRYPTED = Boolean.TRUE;

    private RemComponent remComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        remComponent = DaggerRemComponent.builder()
                .appModule(new AppModule(this))
                .daoModule(new DaoModule(prepareDaoSession()))
                .remModule(new RemModule())
                .build();
    }

    private DaoSession prepareDaoSession()
    {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, ENCRYPTED ?
                "rem-db-encrypted" : "rem-db");
        Database db = ENCRYPTED ?
                helper.getEncryptedWritableDb("super-secret") : helper.getWritableDb();

        DaoSession session = new DaoMaster(db).newSession();
        return session;
    }

    public RemComponent getRemComponent() {
        return remComponent;
    }
}
