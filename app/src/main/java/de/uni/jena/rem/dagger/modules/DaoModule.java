package de.uni.jena.rem.dagger.modules;

import dagger.Module;
import dagger.Provides;
import de.uni.jena.rem.model.DaoSession;
import de.uni.jena.rem.model.TagDao;
import de.uni.jena.rem.model.TaskDao;

/**
 * Created by msc on 07.05.17.
 */
@Module
public class DaoModule
{
    DaoSession session;

    public DaoModule(DaoSession daoSession)
    {
        session = daoSession;
    }

    @Provides
    DaoSession provideDaoSession()
    {
        return this.session;
    }

    @Provides
    TaskDao provideTaskDao(DaoSession session)
    {
       return session.getTaskDao();
    }

    @Provides
    TagDao providesTagDao(DaoSession session)
    {
        return session.getTagDao();
    }
}
