package de.uni.jena.rem.helper;

import java.util.List;

import de.uni.jena.rem.model.Tag;

/**
 * Created by msc on 02.05.17.
 */

public interface TagHelper
{
    List<String> getAvailableTags();

    void saveTag(final Tag tag);

    void deleteTag(final Tag tag);

    Tag findTag(final Long id);
}
