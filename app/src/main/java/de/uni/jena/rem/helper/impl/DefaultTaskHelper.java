package de.uni.jena.rem.helper.impl;

import java.util.List;
import java.util.NoSuchElementException;

import javax.inject.Inject;

import de.uni.jena.rem.adapter.TaskListAdapter;
import de.uni.jena.rem.helper.TaskHelper;
import de.uni.jena.rem.model.Task;
import de.uni.jena.rem.model.TaskDao;
import de.uni.jena.rem.helper.TagHelper;

/**
 * Created by msc on 02.05.17.
 */

public class DefaultTaskHelper implements TaskHelper
{
    private static final String LOG_TAG = DefaultTaskHelper.class.getCanonicalName();

    @Inject TaskListAdapter taskListAdapter;
    @Inject TaskDao taskDao;
    @Inject
    TagHelper tagService;

    @Override
    public void removeTask(Task task)
    {
        taskDao.delete(task);
    }

    @Override
    public void saveTask(Task task)
    {
        taskDao.insertOrReplace(task);
    }

    @Override
    public Task findTask(final Long id) throws NoSuchElementException, IllegalArgumentException
    {
        if(id == null)
        {
            throw new IllegalArgumentException("No valid id was given");
        }
        Task found = taskDao.load(id);

        if(found == null)
        {
            throw new NoSuchElementException("No Task for given id[" + id + "] found!");
        }
        return found;
    }

    public void setTaskListAdapter(TaskListAdapter taskListAdapter) {
        this.taskListAdapter = taskListAdapter;
    }

    @Override
    public List<Task> getAllTasks() {
        return taskDao.loadAll();
    }

    public void setTaskDao(TaskDao taskDao) {
        this.taskDao = taskDao;
    }

    public void setTagService(TagHelper tagService) {
        this.tagService = tagService;
    }
}
