package de.uni.jena.rem.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.NoSuchElementException;

import javax.inject.Inject;

import de.uni.jena.rem.RemApplication;
import de.uni.jena.rem.helper.TaskHelper;
import de.uni.jena.rem.model.Task;
import rem.jena.uni.com.rem.R;

/**
 * Created by msc on 03.05.17.
 */

public class TaskInspectActivity extends Activity
{
    private static final String LOG_TAG = TaskInspectActivity.class.getCanonicalName();

    @Inject
    TaskHelper taskHelper;

    private Task inspectedTask;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((RemApplication) getApplication()).getRemComponent().inject(this);

        setContentView(R.layout.activity_inspect_task);

        final Long taskId = getIntent().getLongExtra(getResources().getString(R.string.intentTaskId), 0);

        try
        {
            this.inspectedTask = taskHelper.findTask(taskId);
            prepareView();
        }
        catch(NoSuchElementException | IllegalArgumentException e)
        {
            Intent i = new Intent(this, TaskListActivity.class);
            startActivity(i);

            Log.e(LOG_TAG, "Error while searching for task with id[" + taskId + "] " +
                    "returning to taskList Activity");
        }
    }

   private void prepareView()
   {
       EditText taskMessage = (EditText) findViewById(R.id.editTaskMessage);
       taskMessage.setText(inspectedTask.getMessage());

       EditText taskDescription = (EditText) findViewById(R.id.editTaskDescription);
       taskDescription.setText(inspectedTask.getDescription());

       EditText taskWifiSSID = (EditText) findViewById(R.id.editWlanSSID);
       taskWifiSSID.setText(inspectedTask.getWlanUid());

       prepareTagSpinner();
   }

   private void prepareTagSpinner()
   {
       Spinner tagSpinner = (Spinner) findViewById(R.id.tagSpinner);
       for(int i = 0; i < tagSpinner.getCount(); i++)
       {
           if(inspectedTask.getTag().getName().equals(tagSpinner.getAdapter().getItem(i)))
           {
               tagSpinner.setSelection(i);
           }
       }
   }

   public void saveTask(View view)
   {
       final View rootView = view.getRootView();

       EditText taskMessage = (EditText) rootView.findViewById(R.id.editTaskMessage);
       EditText taskDescription = (EditText) rootView.findViewById(R.id.editTaskDescription);
       EditText taskWifiSSID = (EditText) findViewById(R.id.editWlanSSID);
       Spinner tagSpinner = (Spinner) rootView.findViewById(R.id.tagSpinner);

       inspectedTask.setMessage(taskMessage.getText().toString());
       inspectedTask.setDescription(taskDescription.getText().toString());
       inspectedTask.setWlanUid(taskWifiSSID.getText().toString());

       Object selectedItem = tagSpinner.getSelectedItem();
       if(selectedItem instanceof String)
       {
           final String selectedTagName = (String) selectedItem;
           if(!selectedTagName.equals(inspectedTask.getTag().getId()))
           {
               inspectedTask.getTag().setName(selectedTagName);
           }
       }

       taskHelper.saveTask(inspectedTask);

       Intent i = new Intent(view.getContext().getApplicationContext(), TaskListActivity.class);
       view.getContext().startActivity(i);
   }
}
