package de.uni.jena.rem.fragment;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.List;

import javax.inject.Inject;

import de.uni.jena.rem.RemApplication;
import de.uni.jena.rem.helper.TagHelper;
import de.uni.jena.rem.model.Task;
import de.uni.jena.rem.helper.TaskHelper;
import rem.jena.uni.com.rem.R;

/**
 * Created by msc on 04.05.17.
 */

public class TagSpinnerFragment extends Fragment
{
    @Inject
    TagHelper tagService;
    @Inject
    TaskHelper taskHelper;

    private Task inspectedTask;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((RemApplication) getActivity().getApplication()).getRemComponent().inject(this);
        return inflater.inflate(R.layout.fragment_task_tagspinner, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        final Context ctx = view.getRootView().getContext();

        List<String> avaiableTags = tagService.getAvailableTags();

        ArrayAdapter<String> tagAdapter = new ArrayAdapter<>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_1, avaiableTags);

        Spinner tagSpinner = (Spinner) getView().findViewById(R.id.tagSpinner);
        if(tagSpinner != null)
        {
            tagSpinner.setAdapter(tagAdapter);
        }
    }
}
