package de.uni.jena.rem.helper;

import java.util.List;

import de.uni.jena.rem.model.Task;

/**
 * Created by msc on 02.05.17.
 */

public interface TaskHelper
{
    Task findTask(final Long id);

    void saveTask(final Task task);

    void removeTask(final Task task);

    List<Task> getAllTasks();
}
