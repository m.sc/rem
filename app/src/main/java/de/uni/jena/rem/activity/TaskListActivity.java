package de.uni.jena.rem.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Spinner;

import java.util.List;

import javax.inject.Inject;

import de.uni.jena.rem.RemApplication;
import de.uni.jena.rem.adapter.TaskListAdapter;
import de.uni.jena.rem.broadcastreceiver.CheckNotificationsBroadcastReceiver;
import de.uni.jena.rem.dialog.CreateTaskDialog;
import de.uni.jena.rem.helper.TagHelper;
import de.uni.jena.rem.listeners.AddTaskButtonOnClickListener;
import de.uni.jena.rem.helper.TaskHelper;
import rem.jena.uni.com.rem.R;

public class TaskListActivity extends Activity
{
    private static String LOG_TAG = TaskListActivity.class.getCanonicalName();

    @Inject
    TaskListAdapter taskListAdapter;
    @Inject
    TaskHelper taskHelper;
    @Inject
    TagHelper tagHelper;
    @Inject
    AlarmManager alarmManager;
    @Inject
    AddTaskButtonOnClickListener addTaskButtonOnClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);

        ((RemApplication) getApplication()).getRemComponent().inject(this);

        prepareTaskGrid();
        prepareTagSpinner();
        prepareAddTaskButton();

        scheduleNotificationChecks();
    }

    private void scheduleNotificationChecks()
    {
        Intent intent = new Intent(getApplicationContext(), CheckNotificationsBroadcastReceiver.class);

        final PendingIntent pIntent = PendingIntent.getBroadcast(this,
                CheckNotificationsBroadcastReceiver.REQUEST_CODE, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        long start = System.currentTimeMillis();
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, start,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES, pIntent);
    }

    private void prepareAddTaskButton()
    {
        Button addTaskButton = (Button) findViewById(R.id.addTaskButton);
        addTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateTaskDialog ctd = new CreateTaskDialog(TaskListActivity.this);
                ctd.setTaskHelper(taskHelper);
                ctd.setTaskListAdapter(taskListAdapter);
                ctd.setTagHelper(tagHelper);
                ctd.show();
            }
        });
    }

    private void prepareTagSpinner()
    {
        Spinner tagSpinner =  (Spinner) findViewById(R.id.tagSpinnerFragment);

        if (tagSpinner != null)
        {
            List<String> availableTags = tagHelper.getAvailableTags();
            if(!availableTags.isEmpty())
            {
                ArrayAdapter<String> tagAdapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_list_item_1, availableTags);
                tagSpinner.setAdapter(tagAdapter);
            }
            else
            {
                //if no available Tags are present remove spinner from view
                ((ViewManager)tagSpinner.getParent()).removeView(tagSpinner);
            }
        }
    }

    private void prepareTaskGrid()
    {
        GridView taskGrid = (GridView) findViewById(R.id.taskgrid);
        taskGrid.setAdapter(taskListAdapter);
    }

    public void setTaskHelper(TaskHelper taskHelper) {
        this.taskHelper = taskHelper;
    }

    public void setTagHelper(TagHelper tagHelper) {
        this.tagHelper = tagHelper;
    }

    public TaskHelper getTaskHelper() {
        return taskHelper;
    }

    public TagHelper getTagHelper() {
        return tagHelper;
    }

    public AlarmManager getAlarmManager() {
        return alarmManager;
    }

    public void setAlarmManager(AlarmManager alarmManager) {
        this.alarmManager = alarmManager;
    }

    public AddTaskButtonOnClickListener getAddTaskButtonOnClickListener() {
        return addTaskButtonOnClickListener;
    }

    public void setAddTaskButtonOnClickListener(AddTaskButtonOnClickListener addTaskButtonOnClickListener) {
        this.addTaskButtonOnClickListener = addTaskButtonOnClickListener;
    }
}
