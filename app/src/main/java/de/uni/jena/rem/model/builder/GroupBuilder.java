package de.uni.jena.rem.model.builder;

import de.uni.jena.rem.model.Group;

/**
 * Created by msc on 16.05.17.
 */

public class GroupBuilder
{
    private String name;

    public GroupBuilder name(String name)
    {
        this.name = name;
        return this;
    }

    public Group build()
    {
        Group group = new Group();
        group.setName(this.name);

        return group;
    }
}
