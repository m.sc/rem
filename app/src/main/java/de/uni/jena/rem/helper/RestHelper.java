package de.uni.jena.rem.helper;

/**
 * Created by msc on 15.06.17.
 */

public interface RestHelper {

    boolean loginUser(String username, String password);

    boolean registerUser(String username, String password);
}
