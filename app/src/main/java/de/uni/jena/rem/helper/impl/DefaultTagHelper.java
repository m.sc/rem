package de.uni.jena.rem.helper.impl;

import java.util.List;

import javax.inject.Inject;

import de.uni.jena.rem.model.Tag;
import de.uni.jena.rem.model.TagDao;
import de.uni.jena.rem.helper.TagHelper;

/**
 * Created by msc on 02.05.17.
 */

public class DefaultTagHelper implements TagHelper
{
    private static final String LOG_TAG = DefaultTagHelper.class.getCanonicalName();

    @Inject List<String> tags;
    @Inject TagDao tagDao;

    @Override
    public void saveTag(Tag tag) {
        tagDao.insertOrReplace(tag);
    }

    @Override
    public void deleteTag(Tag tag) {
        tagDao.delete(tag);
    }

    @Override
    public Tag findTag(Long id) {
        return tagDao.load(id);
    }

    @Override
    public List<String> getAvailableTags()
    {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setTagDao(TagDao tagDao) {
        this.tagDao = tagDao;
    }
}
