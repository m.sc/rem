package de.uni.jena.rem.model.builder;

import de.uni.jena.rem.model.Tag;

/**
 * Created by msc on 02.05.17.
 */

public class TagBuilder
{
    private String name;

    public TagBuilder name(final String name)
    {
        this.name = name;
        return this;
    }

    public Tag build()
    {
        Tag tag = new Tag();
        tag.setName(this.name);

        return tag;
    }
}
