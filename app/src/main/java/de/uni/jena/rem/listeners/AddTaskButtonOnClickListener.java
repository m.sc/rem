package de.uni.jena.rem.listeners;

import android.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import javax.inject.Inject;

import de.uni.jena.rem.activity.TaskListActivity;
import de.uni.jena.rem.adapter.TaskListAdapter;
import de.uni.jena.rem.dialog.CreateTaskDialog;
import de.uni.jena.rem.helper.TagHelper;
import de.uni.jena.rem.model.Tag;
import de.uni.jena.rem.model.Task;
import de.uni.jena.rem.model.builder.TagBuilder;
import de.uni.jena.rem.model.builder.TaskBuilder;
import de.uni.jena.rem.helper.TaskHelper;
import rem.jena.uni.com.rem.R;

/**
 * Created by msc on 03.05.17.
 */

public class AddTaskButtonOnClickListener implements View.OnClickListener
{
    private static final String LOG_TAG = AddTaskButtonOnClickListener.class.getCanonicalName();

    @Inject
    TaskHelper taskHelper;
    @Inject
    TagHelper tagService;
    @Inject TaskListAdapter taskListAdapter;

    @Override
    public void onClick(View view)
    {
        if(view instanceof Button)
        {
            View todoListActivityView = view.getRootView();

            final String taskMessage = ((EditText) todoListActivityView
                    .findViewById(R.id.editTaskMessage)).getText().toString();

            if(taskMessage != null || !taskMessage.isEmpty())
            {
                final Tag selectedTag = getSelectedTag(todoListActivityView);
                final String description = getDescription(todoListActivityView);

                TaskBuilder builder = new TaskBuilder()
                        .message(taskMessage)
                        .tag(selectedTag)
                        .description(description);

                Task createdTask = builder.build();

                taskHelper.saveTask(createdTask);
                taskListAdapter.add(createdTask);

                Log.i(LOG_TAG, "adding Task with message["+ taskMessage +"] and tag[" + selectedTag.getName() + "]");
            }
        }
    }

    private Tag getSelectedTag(View view)
    {
        Tag createdTag = null;
        Spinner tagSpinner = (Spinner) view.findViewById(R.id.tagSpinner);
        if(tagSpinner != null)
        {
            final String tagName  = (String) tagSpinner.getSelectedItem();
            TagBuilder builder = new TagBuilder();
            builder.name(tagName);

            createdTag = builder.build();

            tagService.saveTag(createdTag);
        }
        return createdTag;
    }

    private String getDescription(View view)
    {
        //TODO
        return null;
    }

    public void setTaskHelper(TaskHelper taskHelper) {
        this.taskHelper = taskHelper;
    }

    public void setTagService(TagHelper tagService) {
        this.tagService = tagService;
    }

    public void setTaskListAdapter(TaskListAdapter taskListAdapter) {
        this.taskListAdapter = taskListAdapter;
    }
}
