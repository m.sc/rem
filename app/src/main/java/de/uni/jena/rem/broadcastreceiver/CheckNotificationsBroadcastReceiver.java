package de.uni.jena.rem.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.uni.jena.rem.service.TaskReminderService;

/**
 * Created by msc on 17.07.17.
 */

public class CheckNotificationsBroadcastReceiver extends BroadcastReceiver
{
    public static final int REQUEST_CODE = 12345;

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, TaskReminderService.class));
    }
}
